module Person exposing (Person, decoder)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode


type alias Person =
    { name : String
    , picture : String
    , slack : String
    , participates : Bool
    }


decoder : Decoder Person
decoder =
    Decode.succeed Person
        |> Decode.requiredAt [ "profile", "real_name_normalized" ] Decode.string
        |> Decode.requiredAt [ "profile", "image_512" ] Decode.string
        |> Decode.required "id" Decode.string
        |> Decode.optional "participates" Decode.bool True
