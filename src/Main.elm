port module Main exposing (main)

import Browser
import Calendar exposing (Date)
import Css
import Css.Global
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as HtmlAttributes
import Html.Styled.Events as HtmlEvents
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Levenshtein
import List
import List.Extra as List
import Person exposing (Person)
import Random
import RemoteData exposing (WebData)
import Task exposing (Task)
import Time


port persistUserId : String -> Cmd msg


port forgetUserId : () -> Cmd msg


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view =
            view
                >> viewWrapper
                >> Html.toUnstyled
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { time : Maybe Time.Posix
    , people : WebData (List Person)
    , user : Maybe Person
    , userId : Maybe String
    , userNameInput : String
    , submittedUserName : Maybe String
    , pathname : String
    }



-- INIT


type alias Flags =
    { pathname : String
    , user_id : Maybe String
    }


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { time = Nothing
      , people = RemoteData.Loading
      , user = Nothing
      , userNameInput = ""
      , submittedUserName = Nothing
      , pathname = flags.pathname
      , userId = flags.user_id
      }
    , Cmd.batch
        [ Time.now |> Task.perform GotTime
        , Http.get
            { url = flags.pathname ++ "founders.json"
            , expect =
                Http.expectJson
                    GotFoundersResponse
                    (Decode.list Person.decoder)
            }
        ]
    )



-- UPDATE


type Msg
    = GotTime Time.Posix
    | GotFoundersResponse (Result Http.Error (List Person))
    | UserNameInputChanged String
    | UserNameFormSubmitted
    | UserOptionButtonClicked Person
    | YesItsMeButtonClicked Person
    | NoItsNotMeButtonClicked
    | NoneOfTheAboveButtonClicked
    | IAmNotThisPersonButtonClicked


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotTime time ->
            ( { model
                | time = Just time
              }
            , Cmd.none
            )

        GotFoundersResponse result ->
            ( { model
                | people = RemoteData.fromResult result
                , user =
                    result
                        |> Result.toMaybe
                        |> Maybe.map (List.filter (.slack >> Just >> (==) model.userId))
                        |> Maybe.andThen List.head
              }
            , Cmd.none
            )

        UserNameInputChanged value ->
            ( { model | userNameInput = value }
            , Cmd.none
            )

        UserOptionButtonClicked person ->
            ( { model | user = Just person }
            , person.slack |> persistUserId
            )

        UserNameFormSubmitted ->
            let
                input =
                    String.trim model.userNameInput
            in
            if String.length input < 3 then
                ( model
                , Cmd.none
                )

            else
                ( { model | submittedUserName = Just input }
                , Cmd.none
                )

        YesItsMeButtonClicked person ->
            ( { model | user = Just person }
            , person.slack |> persistUserId
            )

        NoItsNotMeButtonClicked ->
            ( { model | submittedUserName = Nothing }
            , Cmd.none
            )

        NoneOfTheAboveButtonClicked ->
            ( { model | submittedUserName = Nothing }
            , Cmd.none
            )

        IAmNotThisPersonButtonClicked ->
            ( { model
                | submittedUserName = Nothing
                , user = Nothing
                , userNameInput = ""
              }
            , forgetUserId ()
            )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    case model.people of
        RemoteData.NotAsked ->
            Html.text "Oops! Seems like you ran into a bug. Sorry"

        RemoteData.Loading ->
            Html.text "Loading..."

        RemoteData.Failure error ->
            Html.text "Oops! Seems like you ran into a bug. Sorry"

        RemoteData.Success people ->
            case model.user of
                Nothing ->
                    case model.submittedUserName of
                        Nothing ->
                            viewLoginForm model

                        Just name ->
                            viewUserSelection model name

                Just user ->
                    case model.time of
                        Nothing ->
                            Html.text "Oops! Seems like you ran into a bug. Sorry"

                        Just time ->
                            people
                                |> groupPeople time
                                |> List.filter (List.member user)
                                |> List.concat
                                |> viewGroup user


{-| Normalize CSS and add a top-level container
-}
viewWrapper : Html Msg -> Html Msg
viewWrapper element =
    Html.div
        [ HtmlAttributes.css
            [ Css.minHeight <| Css.pct 100
            , Css.justifyContent Css.spaceAround
            , Css.displayFlex
            , Css.alignItems Css.center
            ]
        ]
        [ Css.Global.global
            [ Css.Global.each
                [ Css.Global.body
                , Css.Global.html
                , Css.Global.selector "[data-elm-hot]"
                ]
                [ Css.height <| Css.pct 100
                , Css.margin Css.zero
                , Css.padding Css.zero
                ]
            ]
        , Html.div
            [ HtmlAttributes.css
                [ Css.width <| Css.pct 100
                , Css.maxWidth <| Css.px 960
                , Css.padding <| Css.rem 4
                ]
            ]
            [ element ]
        ]


viewUserSelection : Model -> String -> Html Msg
viewUserSelection model name =
    case model.people of
        RemoteData.NotAsked ->
            Html.text "Oops! Seems like you ran into a bug. Sorry"

        RemoteData.Loading ->
            Html.text "Loading..."

        RemoteData.Failure error ->
            Html.text "Oops! Seems like you ran into a bug. Sorry"

        RemoteData.Success people ->
            case
                people
                    |> List.map (\person -> ( person, nameDistance name person ))
                    |> List.filter (\( _, score ) -> score < 2)
                    |> List.sortBy Tuple.second
                    |> List.map Tuple.first
            of
                [] ->
                    Html.div []
                        [ Html.p
                            [ HtmlAttributes.css
                                [ Css.fontSize <| Css.rem 2
                                ]
                            ]
                            [ "Sorry, I can't find anybody by this name. Please try again." |> Html.text
                            ]
                        , viewUserNameInput model.userNameInput
                        , Html.p
                            []
                            [ "Usually giving your last name (as stated on the Antler Slack) should work. Alternatively contact " |> Html.text
                            , "Tad Lispy"
                                |> Html.text
                                |> List.singleton
                                |> Html.a
                                    [ HtmlAttributes.href "https://app.slack.com/client/T0154MAH99D/D01DD35S354"
                                    , HtmlAttributes.target "_blank"
                                    ]
                            , " for support." |> Html.text
                            ]
                        ]

                [ person ] ->
                    Html.div
                        []
                        [ [ person.name, "?" ]
                            |> String.join ""
                            |> Html.text
                            |> List.singleton
                            |> Html.p
                                [ HtmlAttributes.css
                                    [ Css.fontSize <| Css.rem 2
                                    ]
                                ]
                        , Html.div
                            [ HtmlAttributes.css
                                -- No native CSS grid support: https://github.com/rtfeldman/elm-css/issues/269
                                [ Css.property "display" "grid"
                                , Css.property "gap" "1rem"
                                , Css.property "grid-template-columns" "repeat(auto-fill, 12rem)"
                                ]
                            ]
                            [ "Yes! It's me."
                                |> Html.text
                                |> List.singleton
                                |> Html.button
                                    [ HtmlEvents.onClick (YesItsMeButtonClicked person)
                                    ]
                            , "No, it's not me."
                                |> Html.text
                                |> List.singleton
                                |> Html.button [ HtmlEvents.onClick NoItsNotMeButtonClicked ]
                            ]
                        ]

                matching ->
                    Html.div
                        []
                        [ "Could you please clarify?"
                            |> Html.text
                            |> List.singleton
                            |> Html.p
                                [ HtmlAttributes.css
                                    [ Css.fontSize <| Css.rem 2
                                    ]
                                ]
                        , matching
                            |> List.map viewUserOption
                            |> List.map List.singleton
                            |> List.map
                                (Html.li
                                    [ HtmlAttributes.css
                                        [ Css.margin2
                                            (Css.rem 0.5)
                                            Css.zero
                                        ]
                                    ]
                                )
                            |> Html.ul []
                        , "None of the above."
                            |> Html.text
                            |> List.singleton
                            |> Html.button [ HtmlEvents.onClick NoneOfTheAboveButtonClicked ]
                        ]


viewUserOption : Person -> Html Msg
viewUserOption person =
    Html.button
        [ HtmlEvents.onClick (UserOptionButtonClicked person) ]
        [ Html.text person.name ]


viewLoginForm : Model -> Html Msg
viewLoginForm model =
    let
        viewGreeting =
            Html.p
                [ HtmlAttributes.css
                    [ Css.fontSize <| Css.rem 2
                    ]
                ]
                [ Html.text "Hello! What's your name?" ]
    in
    Html.div
        [ HtmlAttributes.css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.alignSelf Css.center
            ]
        ]
        [ viewGreeting
        , viewUserNameInput model.userNameInput
        ]


viewUserNameInput : String -> Html Msg
viewUserNameInput value =
    Html.form
        [ HtmlEvents.preventDefaultOn "submit" (Decode.succeed ( UserNameFormSubmitted, True ))
        ]
        [ Html.input
            [ HtmlAttributes.type_ "text"
            , HtmlAttributes.value value
            , HtmlEvents.onInput UserNameInputChanged
            , HtmlAttributes.placeholder "Type your name here..."
            ]
            []
        , Html.button [] [ "⮠" |> Html.text ]
        ]


viewGroup : Person -> List Person -> Html Msg
viewGroup user members =
    Html.div
        []
        [ "Here is your lunch party for today:"
            |> Html.text
            |> List.singleton
            |> Html.p
                [ HtmlAttributes.css
                    [ Css.fontSize <| Css.rem 2
                    ]
                ]
        , members
            |> List.map (viewLunchPartner user)
            |> Html.div
                [ HtmlAttributes.css
                    [ Css.displayFlex
                    , Css.flexDirection Css.row
                    , Css.justifyContent Css.spaceAround
                    , Css.flexWrap Css.wrap
                    ]
                ]
        , [ "I am not ", user.name, "!" ]
            |> String.join ""
            |> Html.text
            |> List.singleton
            |> Html.button
                [ HtmlEvents.onClick IAmNotThisPersonButtonClicked
                , HtmlAttributes.css
                    [ Css.margin2
                        (Css.rem 2)
                        Css.zero
                    ]
                ]
        ]


viewLunchPartner : Person -> Person -> Html Msg
viewLunchPartner user partner =
    Html.div
        [ HtmlAttributes.css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.alignItems Css.center
            ]
        ]
        [ partner.name
            |> Html.text
            |> List.singleton
            |> Html.p
                [ HtmlAttributes.css
                    [ Css.fontSize <| Css.rem 3 ]
                ]
        , Html.img
            [ HtmlAttributes.css
                [ Css.width <| Css.pct 100
                , Css.maxWidth <| Css.px 256
                ]
            , HtmlAttributes.src partner.picture
            ]
            []
        , if partner == user then
            "(you)"
                |> Html.text
                |> List.singleton
                |> Html.p []

          else
            "Talk to them on Slack"
                |> Html.text
                |> List.singleton
                |> Html.a
                    [ partner.slack
                        |> String.append "https://foundersams3.slack.com/team/"
                        |> HtmlAttributes.href
                    ]
                |> List.singleton
                |> Html.p []
        ]



-- GROUP PEOPLE


groupPeople : Time.Posix -> List Person -> List (List Person)
groupPeople time people =
    let
        seed : Random.Seed
        seed =
            time
                |> Calendar.fromPosix
                |> Calendar.toMillis
                |> Random.initialSeed
    in
    seed
        |> Random.step (randomOrder people)
        |> Tuple.first
        |> List.greedyGroupsOf 2
        |> List.map (List.filter .participates)
        |> List.sortBy List.length
        |> reassignSingles


reassignSingles : List (List Person) -> List (List Person)
reassignSingles people =
    case people of
        [] ->
            people

        [ group ] ->
            people

        [ single ] :: group :: rest ->
            (single :: group) :: reassignSingles rest

        group :: [ single ] :: rest ->
            (single :: group) :: reassignSingles rest

        a :: b :: rest ->
            a :: reassignSingles (b :: rest)


nameDistance : String -> Person -> Int
nameDistance pattern person =
    let
        name =
            person.name
                |> String.toLower

        search =
            pattern
                |> String.toLower
                |> String.trim
    in
    name
        |> String.split " "
        |> List.filter (String.isEmpty >> not)
        |> List.map (Levenshtein.distance search)
        |> List.minimum
        -- Whatever
        |> Maybe.withDefault 99
        |> Basics.min (Levenshtein.distance search name)



-- RANDOM GENERATORS


randomOrder : List a -> Random.Generator (List a)
randomOrder list =
    Random.list
        (List.length list)
        (Random.float 0 100)
        |> Random.map (List.map2 Tuple.pair list)
        |> Random.map (List.sortBy Tuple.second)
        |> Random.map (List.map Tuple.first)
