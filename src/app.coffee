import { Elm } from "./Main.elm"

node = document.getElementById "app-container"
flags =
    pathname: location.pathname
    user_id: localStorage.getItem "user_id"

app = Elm.Main.init { node, flags }

app.ports.persistUserId.subscribe (user_id) =>
    localStorage.setItem "user_id", user_id

app.ports.forgetUserId.subscribe () =>
    localStorage.removeItem "user_id"
